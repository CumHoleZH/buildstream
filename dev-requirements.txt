coverage == 4.4.0
pep8
pytest >= 3.1.0
pytest-cov >= 2.5.0
pytest-datafiles
pytest-env
pytest-pep8
pytest-pylint
pytest-xdist
